# Sonic the Hedgehog 2

This project is a Arch package that also hosts a setup script that goes along side the RSDK which powers this package. 
The script I have created will automate the setup process for the user when they first launch the application once installed from the Sonicthehedgehog2 package.

You will need to provide the Data.rsdk.xmf from the Sonic The Sonic The Hedgehog 2 Classic Android apk file locationed at ```/assets/Data.rsdk.xmf```
The script will ask for the file when you first launch the Sonic The Hedgehog 2 package

### Auther
 * Corey Bruce
